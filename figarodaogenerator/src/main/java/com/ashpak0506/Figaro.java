package com.ashpak0506;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;
import de.greenrobot.daogenerator.ToOne;


public class Figaro {

    public static void main(String args[]) throws Exception {

        Schema schema = new Schema(1, "com.ashpak0506.model");


        Entity subcategory = schema.addEntity("Subcategory");
        subcategory.addStringProperty("id").primaryKey();
        subcategory.addStringProperty("name").notNull();
        Property rankingProperty = subcategory.addIntProperty("ranking").getProperty();
        subcategory.addBooleanProperty("isVisible");
        // this field should be expose for rest mapping
        Property categoryIdInSub = subcategory.addStringProperty("categoryId").getProperty();

        Entity category = schema.addEntity("Category");
        category.addStringProperty("category").primaryKey();
        ToMany categoryToMany = category.addToMany(subcategory,categoryIdInSub);
        categoryToMany.orderAsc(rankingProperty);
        categoryToMany.setName("subcategories");


        Entity timestamp = schema.addEntity("Timestamp");
        timestamp.addLongProperty("timestamp");
        timestamp.addStringProperty("categoryId").primaryKey();

        Entity articleSubtitle = schema.addEntity("ArticleSubtitle");
        articleSubtitle.addStringProperty("link");
        articleSubtitle.addStringProperty("localLink");
        articleSubtitle.addStringProperty("md5").primaryKey();

        Entity articleHeader = schema.addEntity("ArticleHeader");
        articleHeader.addStringProperty("id").primaryKey();
        articleHeader.addLongProperty("update");
        articleHeader.addLongProperty("date");
        articleHeader.addIntProperty("ranking");
        articleHeader.addStringProperty("title");
        articleHeader.addStringProperty("subtitle");
        articleHeader.addStringProperty("subcategoryId");// not add relation to simplify  program
        Property thumb = articleHeader.addStringProperty("articleSubtitleId").getProperty();
        ToOne articleToOne = articleHeader.addToOne(articleSubtitle, thumb);
        articleToOne.setName("thumb");

        Entity article = schema.addEntity("Article");
        article.addStringProperty("_id").primaryKey();
        article.addStringProperty("author");
        article.addStringProperty("content");
        article.addStringProperty("date");


        new DaoGenerator().generateAll(schema, "../figarodaogenerator/src/main/java/result");
    }
}
