package com.ashpak0506.figaroshpak.controler.ArticleList;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ashpak0506.figaroshpak.FigaroErrorBusEvent;
import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.controler.ArticleList.event.ArticleHeaderEvent;
import com.ashpak0506.figaroshpak.controler.CategoryTab.CategoryTabView;
import com.ashpak0506.figaroshpak.model.ArticleHeader;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.ButterKnife;



/**
 * @author Andriy
 */
public class ArticleListFragment extends ListFragment implements ArticleListView {

    private final static String TAG = "ArticleListFragment";
    private final static String KEY_PARAM = "KEY_PARAM";

    private CategoryTabView activityView;
    private ArticleListPresenter presenter;
    private EventBus bus;

    public static Bundle argument(String param) {
        return new Bundler()
                .putString(KEY_PARAM, param)
                .get();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activityView = (CategoryTabView) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("ArticleListFragment activity must implement CategoryTabView");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String subCategoryId = getArguments().getString(KEY_PARAM);
        if (subCategoryId != null){
            bus =EventBus.getDefault();
            bus.register(this);
            presenter = new ArticleListPresenterImpl(getContext(),this);
            presenter.loadArticleHeader(getContext(),subCategoryId);

        }else {
            Log.e(TAG,"subCategoryId argument equal null");
        }
    }

    @Override
    public void onDestroyView() {
        bus.unregister(this);
        super.onDestroyView();
    }

    @Override
    public void displayArticleHeaderList(List<ArticleHeader> list) {
        ArticleListAdaptor adaptor = new ArticleListAdaptor(getActivity(),list,presenter);
        setListAdapter(adaptor);
    }

    @Override
    public void showProgress(boolean b) {
        activityView.showProgress(b);
    }

    @Override
    public void displayError(int errMsgId) {
        activityView.displayError(errMsgId);
    }

    @Override
    public void displayMsg(int msgId) {
        activityView.displayMsg(msgId);
    }

    @Subscribe
    public void onEvent(ArticleHeaderEvent event) {
        presenter.onEvent(event);
    }

    @Subscribe
    public void onErrorEvent(FigaroErrorBusEvent event) {
        presenter.onErrorEvent(event);
    }



}
