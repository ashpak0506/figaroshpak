package com.ashpak0506.figaroshpak.controler.Article;

import android.content.Context;
import android.widget.ImageView;

/**
 * @author Andriy
 */
public interface ArticlePresenter {

    void loadArticle(Context context, String articleId);
    void loadImage(Context context ,String url , ImageView out);

}
