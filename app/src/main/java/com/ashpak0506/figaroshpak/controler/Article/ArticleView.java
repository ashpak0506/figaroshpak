package com.ashpak0506.figaroshpak.controler.Article;

import com.ashpak0506.figaroshpak.model.Article;

/**
 * @author Andriy
 */
@SuppressWarnings("SameParameterValue")
interface ArticleView {
    void showProgress(boolean show);
    void displayError(int errMsgId);
    void displayMsg(int msgId);
    void displayArticle(Article article);
}
