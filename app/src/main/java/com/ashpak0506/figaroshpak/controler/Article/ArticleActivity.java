package com.ashpak0506.figaroshpak.controler.Article;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.controler.BaseActivity;
import com.ashpak0506.figaroshpak.model.Article;
import com.ashpak0506.figaroshpak.util.AppUtil;
import com.ashpak0506.figaroshpak.util.MsgUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;

/*
 *  Display one article
 *  @author Andriy
 */
public class ArticleActivity extends BaseActivity implements ArticleView {

    public static final String ARTICLE_ID = "ARTICLE_ID";
    public static final String ARTICLE_IMAGE = "ARTICLE_IMAGE";

    @Bind(R.id.article_main_layout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.article_content)
    View articleContent;
    @Bind(R.id.article_progress)
    ProgressBar progressBar;
    @Bind(R.id.article_author)
    TextView author;
    @Bind(R.id.content_web_view)
    WebView webView;
    @Bind(R.id.article_image)
    ImageView imageView;
    @Bind(R.id.err_content)
    View err404;

    private ArticlePresenter presenter;
    @State
    String imgUrl;
    @State
    String articleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        ButterKnife.bind(this);
        Icepick.restoreInstanceState(this, savedInstanceState);

        setupToolbar();
        setupWebView();

        if (articleId == null)
            articleId = getIntent().getStringExtra(ARTICLE_ID);
        if (imgUrl == null)
            imgUrl = getIntent().getStringExtra(ARTICLE_IMAGE);

        presenter = new ArticlePresenterImpl(this);
        presenter.loadArticle(this, articleId);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void showProgress(boolean show) {
        showProgress(articleContent, progressBar, show);
    }

    @Override
    public void displayError(int errId) {
        MsgUtil.buildErrorSnack(this, coordinatorLayout, errId).show();
        err404.setVisibility(View.VISIBLE);
        articleContent.setVisibility(View.GONE);
    }

    @Override
    public void displayMsg(int msgId) {
        MsgUtil.buildSnack(this, coordinatorLayout, msgId).show();
    }


    @Override
    public void displayArticle(Article article) {
        if (!AppUtil.hasNull(article, article.getAuthor(), article.getContent())) {
            err404.setVisibility(View.GONE);
            articleContent.setVisibility(View.VISIBLE);
            if (imgUrl != null)
                presenter.loadImage(this, imgUrl, imageView);
            author.setText(article.getAuthor());
            webView.loadData(article.getContent(), "text/html; charset=utf-8", "utf-8");
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null)
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setupWebView() {
        webView.getSettings().setDefaultTextEncodingName("utf-8");
    }
}
