package com.ashpak0506.figaroshpak.controler.ArticleList;

import com.ashpak0506.figaroshpak.model.ArticleHeader;

import java.util.List;

/**
 * @author Andriy
 */
@SuppressWarnings("SameParameterValue")
interface ArticleListView {
    void showProgress(boolean b);
    void displayError(int errMsgId);
    void displayMsg(int msgId);
    void displayArticleHeaderList(List<ArticleHeader> list);
}
