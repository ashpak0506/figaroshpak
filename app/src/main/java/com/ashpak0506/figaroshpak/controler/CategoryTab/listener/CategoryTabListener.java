package com.ashpak0506.figaroshpak.controler.CategoryTab.listener;

import com.ashpak0506.figaroshpak.model.Category;

import java.util.List;

/**
 * @author Andriy
 */
public interface CategoryTabListener {
    void onSuccess(boolean local , List<Category> categories);
    void onError();
}
