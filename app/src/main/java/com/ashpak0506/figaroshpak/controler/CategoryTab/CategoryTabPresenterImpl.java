package com.ashpak0506.figaroshpak.controler.CategoryTab;

import android.content.Context;

import com.ashpak0506.figaroshpak.App;
import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.controler.CategoryTab.listener.CategoryTabListener;
import com.ashpak0506.figaroshpak.iteractor.CategoryInteractor;
import com.ashpak0506.figaroshpak.iteractor.CategoryInteractorImpl;
import com.ashpak0506.figaroshpak.model.Category;

import java.util.List;

/**
 * @author Andriy
 */
public class CategoryTabPresenterImpl implements CategoryTabPresenter {

    private final CategoryTabView view;

    public CategoryTabPresenterImpl(CategoryTabView view){
        this.view = view;
    }

    @Override
    public void loadCategory(Context context) {
        CategoryInteractor interactor = new CategoryInteractorImpl(App.get(context));
        view.showProgress(true);
        interactor.getAllCategory(onGetAllCategory);
    }


    private final CategoryTabListener onGetAllCategory = new CategoryTabListener() {

        @Override
        public void onSuccess(boolean local , List<Category> categories) {
            view.showProgress(false);
            view.displayTabs(categories);
            if(local)
                view.displayMsg(R.string.cache_data_msg);
        }

        @Override
        public void onError() {
            view.showProgress(false);
            view.displayError(R.string.common_error);
        }
    };

}
