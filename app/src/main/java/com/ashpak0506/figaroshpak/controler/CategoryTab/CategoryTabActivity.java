package com.ashpak0506.figaroshpak.controler.CategoryTab;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.controler.ArticleList.ArticleListFragment;
import com.ashpak0506.figaroshpak.controler.BaseActivity;
import com.ashpak0506.figaroshpak.model.Category;
import com.ashpak0506.figaroshpak.util.MsgUtil;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CategoryTabActivity extends BaseActivity implements CategoryTabView {

    @Bind(R.id.category_tab_progress)
    View progressBar;
    @Bind(R.id.category_tab_layout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.viewpagertab)
    SmartTabLayout viewPagerTab;
    @Bind(R.id.viewpager)
    ViewPager viewPager;

    private CategoryTabPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_tab);
        ButterKnife.bind(this);
        presenter = new CategoryTabPresenterImpl(this);
        presenter.loadCategory(this);

    }


    @Override
    public void displayTabs(List<Category> category) {
        FragmentPagerItems items = buildPagersItems(category);
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), items);
        viewPager.setAdapter(adapter);
        viewPagerTab.setViewPager(viewPager);
    }

    @Override
    public void showProgress(boolean show) {
        showProgress(viewPager, progressBar, show);
    }

    @Override
    public void displayError(int errId) {
        MsgUtil.buildErrorSnack(this, coordinatorLayout, errId).show();
    }

    @Override
    public void displayMsg(int msgId) {
        MsgUtil.buildSnack(this, coordinatorLayout, msgId).show();
    }


    private FragmentPagerItems buildPagersItems(List<Category> categories) {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(this);
        for (Category category : categories) {
            String param = getSubcategoryId(category);
            creator.add(category.getCategory(), ArticleListFragment.class
                    , ArticleListFragment.argument(param));
        }
        return creator.create();
    }

    private String getSubcategoryId(Category category) {
        String id = null;
        if (category != null && category.getSubcategories().size() > 0) {
            id = category.getSubcategories().get(0).getId();
        }
        return id;
    }

}
