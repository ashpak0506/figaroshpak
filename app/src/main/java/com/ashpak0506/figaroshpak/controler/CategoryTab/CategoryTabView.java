package com.ashpak0506.figaroshpak.controler.CategoryTab;

import com.ashpak0506.figaroshpak.model.Category;

import java.util.List;

/**
 * @author Andriy
 */
public interface CategoryTabView  {
    void showProgress(boolean b);
    void displayError(int errMsgId);
    void displayMsg(int msgId);
    void displayTabs(List<Category> category);
}
