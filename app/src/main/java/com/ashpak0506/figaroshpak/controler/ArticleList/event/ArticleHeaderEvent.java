package com.ashpak0506.figaroshpak.controler.ArticleList.event;

import com.ashpak0506.figaroshpak.model.ArticleHeader;

import java.util.List;

/**
 * @author Andriy
 */
public class ArticleHeaderEvent {
    private final boolean cache;
    private final List<ArticleHeader> articleHeaders;

    public ArticleHeaderEvent(boolean cache,List<ArticleHeader> articleHeaders){
        this.cache = cache;
        this.articleHeaders = articleHeaders;
    }

    public boolean isCache() {
        return cache;
    }


    public List<ArticleHeader> getArticleHeaders() {
        return articleHeaders;
    }

}
