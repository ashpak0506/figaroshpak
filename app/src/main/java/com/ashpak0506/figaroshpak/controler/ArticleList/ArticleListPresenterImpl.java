package com.ashpak0506.figaroshpak.controler.ArticleList;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.ashpak0506.figaroshpak.App;
import com.ashpak0506.figaroshpak.FigaroErrorBusEvent;
import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.controler.Article.ArticleActivity;
import com.ashpak0506.figaroshpak.controler.ArticleList.event.ArticleHeaderEvent;
import com.ashpak0506.figaroshpak.iteractor.ArticleHeaderInteractor;
import com.ashpak0506.figaroshpak.iteractor.ArticleHeaderInteractorImpl;
import com.ashpak0506.figaroshpak.model.ArticleHeader;
import com.ashpak0506.figaroshpak.model.ArticleSubtitle;
import com.ashpak0506.figaroshpak.util.AppUtil;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.lang.ref.WeakReference;

/**
 * @author Andriy
 */
public class ArticleListPresenterImpl implements ArticleListPresenter {

    private final static String GAG_IMAGE= "%dx%d";

    private final ArticleListView view;
    private final String preferableImageSize;
    private ArticleHeaderInteractor interactor;


    public ArticleListPresenterImpl(Context context,ArticleListView view) {
        this.view = view;
        preferableImageSize = context.getString(R.string.preferable_image_size);

    }

    @Override
    public void loadArticleHeader(Context context, String subCategoryId) {
        interactor = new ArticleHeaderInteractorImpl(App.get(context));
        interactor.getArticleHeaders(subCategoryId);
    }

    @Override
    public void onArticleChoose(Context context, String articleId,String imagePath) {
        Intent intent = new Intent(context, ArticleActivity.class);
        intent.putExtra(ArticleActivity.ARTICLE_ID, articleId);
        intent.putExtra(ArticleActivity.ARTICLE_IMAGE, imagePath);
        context.startActivity(intent);
    }

    @Override
    public void loadImage(Context context, ArticleHeader header, ImageView out) {
        try {
            String path;
            ArticleSubtitle thumb= header.getSimpleThumb();
            if (!AppUtil.hasNull(thumb,thumb.getLink())) {

                if (thumb.getLocalLink() != null) {
                    path = thumb.getLocalLink();
                    Picasso.with(context)
                            .load(path)
                            .into(out);
                } else {
                    Target target = new ArticleImageTarget(header.getTitle() + header.getId()
                            , context.getContentResolver()
                            , header
                            , out);
                    path = thumb.getLink();
                    path = path.replace(GAG_IMAGE,preferableImageSize);
                    Picasso.with(context).load(path).into(target);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onEvent(ArticleHeaderEvent event) {
        view.showProgress(false);
        if (!AppUtil.hasNull(event, event.getArticleHeaders())) {
            view.displayArticleHeaderList(event.getArticleHeaders());
            if (event.isCache())
                view.displayMsg(R.string.cache_data_msg);
        }
    }

    @Override
    public void onErrorEvent(FigaroErrorBusEvent event) {
        view.showProgress(false);
        if (!AppUtil.hasNull(event, event.getKey())) {
            if (event.getKey().equals(FigaroErrorBusEvent.ErrorKey.ARTICLE_HEADER_INTERACTION)) {
                view.displayError(R.string.common_error);
            }
        }
    }


    private   class ArticleImageTarget implements Target {

        private final String TAG = "Figaro";
        private final WeakReference<ContentResolver> resolver;

        private final String name;
        private final ImageView imageView;
        private final ArticleHeader outHeader;

        public ArticleImageTarget(String name, ContentResolver resolver, ArticleHeader outHeader, ImageView outImage) {
            this.resolver = new WeakReference<>(resolver);
            this.name = name;
            this.imageView = outImage;
            this.outHeader = outHeader;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            imageView.setImageBitmap(bitmap);
            ContentResolver r = resolver.get();
            if (r != null) {
                String url = MediaStore.Images.Media.insertImage(r, bitmap, name, TAG);
                outHeader.getSimpleThumb().setLocalLink(url);
                interactor.update(outHeader);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }

}
