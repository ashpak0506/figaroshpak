package com.ashpak0506.figaroshpak.controler.ArticleList;

import android.content.Context;
import android.widget.ImageView;

import com.ashpak0506.figaroshpak.FigaroErrorBusEvent;
import com.ashpak0506.figaroshpak.controler.ArticleList.event.ArticleHeaderEvent;
import com.ashpak0506.figaroshpak.model.ArticleHeader;

/**
 * @author Andriy
 */
interface ArticleListPresenter {
    void loadArticleHeader(Context context, String substringId);

    void onArticleChoose(Context context, String articleId , String imagePath);

    void loadImage(Context context, ArticleHeader header, ImageView out);

    void onEvent(ArticleHeaderEvent event);

    void onErrorEvent(FigaroErrorBusEvent event);
}
