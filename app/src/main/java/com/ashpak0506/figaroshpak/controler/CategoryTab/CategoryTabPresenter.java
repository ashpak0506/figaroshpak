package com.ashpak0506.figaroshpak.controler.CategoryTab;

import android.content.Context;

/**
 * @author Andriy
 */
interface CategoryTabPresenter {
        void loadCategory(Context context);
}
