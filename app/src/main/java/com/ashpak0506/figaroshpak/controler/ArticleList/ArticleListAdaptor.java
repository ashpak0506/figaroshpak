package com.ashpak0506.figaroshpak.controler.ArticleList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.model.ArticleHeader;
import com.ashpak0506.figaroshpak.model.ArticleSubtitle;

import java.util.List;

/**
 * Display article header element
 * @author Andriy
 */

@SuppressWarnings("CanBeFinal")
class ArticleListAdaptor extends ArrayAdapter<ArticleHeader> {

    private final static int RESOURCE = R.layout.article_list_element;
    private Context context;
    private List<ArticleHeader> articleHeaders;
    private ArticleListPresenter presenter;

    public ArticleListAdaptor(Context context, List<ArticleHeader> articleHeaders, ArticleListPresenter presenter) {
        super(context, RESOURCE, articleHeaders);
        this.context = context;
        this.articleHeaders = articleHeaders;
        this.presenter = presenter;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(RESOURCE, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        ArticleHeader articleHeader = articleHeaders.get(position);
        viewHolder.position = position;
        viewHolder.title.setText(articleHeader.getTitle());
        viewHolder.subtitle.setText(articleHeader.getSubtitle());
        presenter.loadImage(context, articleHeader, viewHolder.imageView);

        view.setOnClickListener(onItemClick);

        return view;
    }


    private final View.OnClickListener onItemClick = v -> {
        String url = null;
        int pos = ((ViewHolder) v.getTag()).position;
        ArticleHeader header = articleHeaders.get(pos);
        ArticleSubtitle thumb = header.getSimpleThumb();
        if (thumb != null)
            url = thumb.getLocalLink() == null ? thumb.getLink() : thumb.getLocalLink();
        presenter.onArticleChoose(context, header.getId(), url);
    };


    public class ViewHolder {

        public int position;

        public final TextView title;
        public final TextView subtitle;
        public final ImageView imageView;

        public ViewHolder(View v) {
            title = (TextView) v.findViewById(R.id.item_title);
            subtitle = (TextView) v.findViewById(R.id.item_subtitle);
            imageView = (ImageView) v.findViewById(R.id.item_image);
        }
    }
}