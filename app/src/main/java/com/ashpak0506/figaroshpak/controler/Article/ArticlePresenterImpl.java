package com.ashpak0506.figaroshpak.controler.Article;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.ImageView;

import com.ashpak0506.figaroshpak.App;
import com.ashpak0506.figaroshpak.FigaroCommonListener;
import com.ashpak0506.figaroshpak.R;
import com.ashpak0506.figaroshpak.iteractor.ArticleInteractor;
import com.ashpak0506.figaroshpak.iteractor.ArticleInteractorImpl;
import com.ashpak0506.figaroshpak.model.Article;

import org.parceler.Parcels;

/**
 * Get article from interactor and say view to display
 * @author Andriy
 */
public class ArticlePresenterImpl implements ArticlePresenter {

    private final ArticleView view;
    ArticleInteractor interactor;

    public ArticlePresenterImpl(ArticleView view){
        this.view = view;
    }


    @Override
    public void loadArticle(Context context, String articleId) {
        view.showProgress(true);
        interactor = new ArticleInteractorImpl(App.get(context));
        interactor.getArticle(articleId,listener);
    }

    public void loadImage(Context context ,String imgUrl , ImageView out) {
        interactor.loadImage(context,imgUrl,out);
    }

    /*Common listener use in case small amount of date(Not create to many listener)*/
    private final FigaroCommonListener listener = new FigaroCommonListener() {
        @Override
        public void onError(Bundle bundle, String... errId) {
            view.showProgress(false);
            view.displayError(R.string.common_error);
        }

        @Override
        public void onSuccess(Bundle bundle, String... id) {
            view.showProgress(false);
            Parcelable parcelable = bundle.getParcelable(id[0]);
            Article article = Parcels.unwrap(parcelable);
            boolean cache = bundle.getBoolean(id[1],false);
            view.displayArticle(article);
            if (cache)
                view.displayMsg(R.string.cache_data_msg);
        }
    };
}
