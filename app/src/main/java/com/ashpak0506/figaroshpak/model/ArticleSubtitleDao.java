package com.ashpak0506.figaroshpak.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "ARTICLE_SUBTITLE".
*/
@SuppressWarnings("ALL")
public class ArticleSubtitleDao extends AbstractDao<ArticleSubtitle, String> {

    public static final String TABLENAME = "ARTICLE_SUBTITLE";

    /**
     * Properties of entity ArticleSubtitle.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Link = new Property(0, String.class, "link", false, "LINK");
        public final static Property LocalLink = new Property(1, String.class, "localLink", false, "LOCAL_LINK");
        public final static Property Md5 = new Property(2, String.class, "md5", true, "MD5");
    }


    public ArticleSubtitleDao(DaoConfig config) {
        super(config);
    }
    
    public ArticleSubtitleDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"ARTICLE_SUBTITLE\" (" + //
                "\"LINK\" TEXT," + // 0: link
                "\"LOCAL_LINK\" TEXT," + // 1: localLink
                "\"MD5\" TEXT PRIMARY KEY NOT NULL );"); // 2: md5
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"ARTICLE_SUBTITLE\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, ArticleSubtitle entity) {
        stmt.clearBindings();
 
        String link = entity.getLink();
        if (link != null) {
            stmt.bindString(1, link);
        }
 
        String localLink = entity.getLocalLink();
        if (localLink != null) {
            stmt.bindString(2, localLink);
        }
 
        String md5 = entity.getMd5();
        if (md5 != null) {
            stmt.bindString(3, md5);
        }
    }

    /** @inheritdoc */
    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2);
    }    

    /** @inheritdoc */
    @Override
    public ArticleSubtitle readEntity(Cursor cursor, int offset) {
        ArticleSubtitle entity = new ArticleSubtitle( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // link
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // localLink
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2) // md5
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, ArticleSubtitle entity, int offset) {
        entity.setLink(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setLocalLink(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setMd5(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
     }
    
    /** @inheritdoc */
    @Override
    protected String updateKeyAfterInsert(ArticleSubtitle entity, long rowId) {
        return entity.getMd5();
    }
    
    /** @inheritdoc */
    @Override
    public String getKey(ArticleSubtitle entity) {
        if(entity != null) {
            return entity.getMd5();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
