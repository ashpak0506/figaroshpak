package com.ashpak0506.figaroshpak;

import android.os.Bundle;

/**
 *
 * General listener interface with bundle
 * for all observer class in app
 *
 * @author Andriy
 */
public interface FigaroCommonListener {
    void onError(Bundle bundle , String... errId);
    void onSuccess(Bundle bundle , String... id);
}
