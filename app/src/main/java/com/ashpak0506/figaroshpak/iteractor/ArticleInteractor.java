package com.ashpak0506.figaroshpak.iteractor;

import android.content.Context;
import android.widget.ImageView;

import com.ashpak0506.figaroshpak.FigaroCommonListener;

public interface ArticleInteractor {
    void getArticle(String articleId, FigaroCommonListener listener);
    void loadImage(Context context , String imgUrl , ImageView out);
}
