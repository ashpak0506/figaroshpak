package com.ashpak0506.figaroshpak.iteractor;

import com.ashpak0506.figaroshpak.model.ArticleHeader;

/**
 * @author Andriy
 */
public interface ArticleHeaderInteractor {
    void getArticleHeaders(String categoryId);
    void update(ArticleHeader header);
}
