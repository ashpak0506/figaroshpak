package com.ashpak0506.figaroshpak.iteractor;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.ashpak0506.figaroshpak.App;
import com.ashpak0506.figaroshpak.FigaroCommonListener;
import com.ashpak0506.figaroshpak.iteractor.rest.ArticleRestApi;
import com.ashpak0506.figaroshpak.model.Article;
import com.ashpak0506.figaroshpak.model.ArticleDao;
import com.ashpak0506.figaroshpak.model.DaoSession;
import com.ashpak0506.figaroshpak.util.AppUtil;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Try to load article with image from Rest
 * in error try load from database
 * @author Andriy
 */
public class ArticleInteractorImpl extends BaseInteractor implements ArticleInteractor {

    private final static String TAG = "ArticleInteractorImpl";
    private ArticleDao articleDao;

    public ArticleInteractorImpl(App app) {
        DaoSession daoSession = app.getDaoSession();
        articleDao = daoSession.getArticleDao();
    }

    @Override
    public void loadImage(Context context , String imgUrl , ImageView out) {
        try {
            Picasso.with(context)
                    .load(imgUrl)
                    .into(out);
        } catch (Exception e) {
            Log.e(TAG,e.toString());
        }
    }

    @Override
    public void getArticle(String articleId, FigaroCommonListener listener) {
        ArticleRestApi restApi = retrofitBuild().create(ArticleRestApi.class);
        Call<Article> call = restApi.getArticle(articleId);
        call.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Response<Article> response) {
                if (!AppUtil.hasNull(response, response.body())) {
                    getArticleSuccess(listener, response.body(),false);
                } else {
                    getArticleError(articleId, listener);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.getMessage());
                getArticleError(articleId, listener);
            }
        });

    }


    private void getArticleSuccess(FigaroCommonListener listener, Article article,boolean cache) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("article", Parcels.wrap(article));
        bundle.putBoolean("cache",cache);
        listener.onSuccess(bundle, "article","cache");
        articleDao.insertOrReplace(article);
    }


    private void getArticleError(String articleId, FigaroCommonListener listener) {

        Article article = articleDao.selectById(articleId);
        if (article != null) {
            Log.e(TAG,"Can't get getArticle from db");
            getArticleSuccess(listener, article,true);
        } else {
            Log.d(TAG,"Get getArticle from db");
            listener.onError(null, "");
        }
    }


}
