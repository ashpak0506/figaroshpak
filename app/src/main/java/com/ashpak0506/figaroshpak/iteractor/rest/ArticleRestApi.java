package com.ashpak0506.figaroshpak.iteractor.rest;

import com.ashpak0506.figaroshpak.model.Article;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * @author Andriy
 */
public interface ArticleRestApi {

    @GET("/article/header/{categoryId}")
    Call<ResponseBody> getArticleHeader(@Path("categoryId") String categoryId);

    @GET("/article/{articleId}")
    Call<Article> getArticle(@Path("articleId") String articleId);

}
