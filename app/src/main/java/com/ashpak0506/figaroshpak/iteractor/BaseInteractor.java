package com.ashpak0506.figaroshpak.iteractor;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 *
 * Interactor base class
 * build Retrofit http adapter
 *
 * @author Andriy
 */
abstract public class BaseInteractor {

    private final static int CONNECT_TIMEOUT = 3;


    protected Retrofit retrofitBuild(){
            return  buildSimpleRetrofit(ServerUrl.BASE_SERVER_URL);
    }


    @SuppressWarnings("SameParameterValue")
    private Retrofit buildSimpleRetrofit(String baseUrl){
        Gson gson = buildGson();
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(new OkHttpClient().newBuilder().connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS).build())
                .build();
    }


    /*Exclude expose field from model excludeFieldsWithoutExposeAnnotation()*/
    private Gson buildGson(){
        return new GsonBuilder().create();
    }


    protected static class ServerUrl{
        private final static String BASE_SERVER_URL = "http://figaro.service.yagasp.com/";
    }

}
