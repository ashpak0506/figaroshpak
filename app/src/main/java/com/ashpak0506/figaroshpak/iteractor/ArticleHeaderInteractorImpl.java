package com.ashpak0506.figaroshpak.iteractor;

import android.support.v4.util.Pair;
import android.util.Log;

import com.ashpak0506.figaroshpak.App;
import com.ashpak0506.figaroshpak.FigaroErrorBusEvent;
import com.ashpak0506.figaroshpak.controler.ArticleList.event.ArticleHeaderEvent;
import com.ashpak0506.figaroshpak.iteractor.rest.ArticleRestApi;
import com.ashpak0506.figaroshpak.model.ArticleHeader;
import com.ashpak0506.figaroshpak.model.ArticleHeaderDao;
import com.ashpak0506.figaroshpak.model.DaoSession;
import com.ashpak0506.figaroshpak.model.Timestamp;
import com.ashpak0506.figaroshpak.model.TimestampDao;
import com.ashpak0506.figaroshpak.util.AppUtil;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Try to load article header with thumb from Rest
 * if timestamp change reload
 * in error try load from database
 *
 * Parse manual article header response to timestamp model
 * and list of article header model
 *
 * @author Andriy
 */
public class ArticleHeaderInteractorImpl extends BaseInteractor implements ArticleHeaderInteractor {


    private final static String TAG = "ArticleHeaderInt";

    private TimestampDao timestampDao;
    private ArticleHeaderDao articleHeaderDao;
    private EventBus bus;

    public ArticleHeaderInteractorImpl(App app) {
        DaoSession daoSession = app.getDaoSession();
        timestampDao = daoSession.getTimestampDao();
        articleHeaderDao = daoSession.getArticleHeaderDao();
        bus = EventBus.getDefault();
    }


    @Override
    public void getArticleHeaders(String subCategoryId) {

        ArticleRestApi restApi = retrofitBuild().create(ArticleRestApi.class);
        Call<ResponseBody> call = restApi.getArticleHeader(subCategoryId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                String json = stringFromResponseBody(response.body());
                if (!AppUtil.hasNull(response, response.body())) {
                    Pair<Timestamp, List<ArticleHeader>> result = parseArticleHeaderResponse(json);
                    result.first.setCategoryId(subCategoryId);
                    populateArticleHeaderWithSubcategory(subCategoryId,result.second);
                    getArticleHeadersSuccess(subCategoryId,result,false);
                    articleHeaderDao.insertOrReplaceTdCascade(result.second);
                } else {
                    getArticleHeadersError(subCategoryId);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                getArticleHeadersError(subCategoryId);
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void update(ArticleHeader header) {
        articleHeaderDao.update(header);
    }


    private void getArticleHeadersSuccess(String subCategoryId , Pair<Timestamp,List<ArticleHeader>> result,boolean cache) {

        if (isNewArticleHeader(subCategoryId,result.first)) {
            if (result.second != null) {
                timestampDao.insertOrReplace(result.first);
                ArticleHeaderEvent event = new ArticleHeaderEvent(cache,result.second);
                bus.post(event);
            }
        }
    }

    private void getArticleHeadersError(String subCategoryId) {
        List<ArticleHeader> list = articleHeaderDao.loadBySubcategoryId(subCategoryId);
        Timestamp timestamp = timestampDao.load(subCategoryId);

        if(!AppUtil.hasNull(list,timestamp)){
            getArticleHeadersSuccess(subCategoryId,new Pair<>(timestamp,list),true);
            Log.e(TAG,"Get ArticleHeaders from db");
        }else {
            FigaroErrorBusEvent event = new FigaroErrorBusEvent();
            event.setKey(FigaroErrorBusEvent.ErrorKey.ARTICLE_HEADER_INTERACTION);
            bus.post(event);
            Log.e(TAG,"Can't get ArticleHeaders from db");
        }
    }

    private String stringFromResponseBody(ResponseBody responseBody) {
        String json;
        try {
            json = responseBody.string();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            json = null;
        }
        return json;
    }


    private boolean isNewArticleHeader(String subCategoryId,Timestamp timestamp) {
        Timestamp current = timestampDao.load(subCategoryId);
        return timestamp != null && (current == null || timestamp.getTimestamp() > current.getTimestamp());
    }


    private Pair<Timestamp, List<ArticleHeader>> parseArticleHeaderResponse(String json) {
        Type type = new TypeToken<List<ArticleHeader>>() {}.getType();
        Gson gson = new Gson();
        //split json on timestamp model json and article header json
        String timestampJson = parseTimestamp(json);
        String articleHeaderJson = parseArticleHeader(json);
        Timestamp timestamp = null;
        List<ArticleHeader> list = null;
        try {
            timestamp = gson.fromJson(timestampJson, Timestamp.class);
            list = gson.fromJson(articleHeaderJson, type);
        } catch (JsonSyntaxException e) {
           Log.e(TAG,e.toString());
        }

        return new Pair<>(timestamp,list);
    }

    private String parseTimestamp(String json){
        int begin = json.indexOf("{\"timestamp\"");
        int end = json.indexOf("}]");
        return json.substring(begin,end + 1);
    }

    private String parseArticleHeader(String json){
        int begin = json.indexOf("[{\"id\"");
        return json.substring(begin,json.length()-1);
    }

    private void populateArticleHeaderWithSubcategory(String subCategory,List<ArticleHeader> inOut){
        for (ArticleHeader header : inOut){
            header.setSubcategoryId(subCategory);
        }
    }

}
