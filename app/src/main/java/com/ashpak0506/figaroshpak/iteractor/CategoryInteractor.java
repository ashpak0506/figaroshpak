package com.ashpak0506.figaroshpak.iteractor;

import com.ashpak0506.figaroshpak.controler.CategoryTab.listener.CategoryTabListener;

/**
 * @author Andriy
 */
public interface CategoryInteractor {
     void getAllCategory(CategoryTabListener listener);
}
