package com.ashpak0506.figaroshpak.iteractor.rest;

import com.ashpak0506.figaroshpak.model.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author Andriy
 */
public interface CategoryRestApi {

    @GET("article/categories")
    Call<List<Category>> getAllCategory();

}
