package com.ashpak0506.figaroshpak.iteractor;

import android.util.Log;

import com.ashpak0506.figaroshpak.App;
import com.ashpak0506.figaroshpak.controler.CategoryTab.listener.CategoryTabListener;
import com.ashpak0506.figaroshpak.iteractor.rest.CategoryRestApi;
import com.ashpak0506.figaroshpak.model.Category;
import com.ashpak0506.figaroshpak.model.CategoryDao;
import com.ashpak0506.figaroshpak.model.DaoSession;
import com.ashpak0506.figaroshpak.util.AppUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Try to load category with subcategory from Rest
 * in error try load from database
 * @author Andriy
 */
public class CategoryInteractorImpl extends BaseInteractor implements CategoryInteractor {

    private final static String TAG = "CategoryInteractorImpl";
    private final CategoryDao categoryDao;

    public CategoryInteractorImpl(App app) {
        DaoSession daoSession = app.getDaoSession();
        categoryDao = daoSession.getCategoryDao();
    }

    @Override
    public void getAllCategory(CategoryTabListener listener) {
        CategoryRestApi categoryRestApi = retrofitBuild().create(CategoryRestApi.class);
        Call<List<Category>> call = categoryRestApi.getAllCategory();
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Response<List<Category>> response) {
                if (!AppUtil.hasNull(response, response.body())) {
                    getAllCategoryOnSuccess(listener, response.body(),false);
                    categoryDao.insertOrReplaceCascade(response.body());
                } else {
                    getAllCategoryOnError(listener);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                getAllCategoryOnError(listener);
            }
        });
    }


    private List<Category> getAllCategoryFromDao() {
        return categoryDao.queryBuilder().list();
    }


    private void getAllCategoryOnSuccess(CategoryTabListener listener, List<Category> categories,boolean local) {
        listener.onSuccess(local,categories);
    }

    private void getAllCategoryOnError(CategoryTabListener listener) {

        List<Category> categories = getAllCategoryFromDao();
        if (categories == null) {
            Log.e(TAG,"Can't get getAllCategory from db");
            listener.onError();
        } else {
            Log.e(TAG,"Get getAllCategory from db");
            getAllCategoryOnSuccess(listener, categories,true);
        }
    }


}
