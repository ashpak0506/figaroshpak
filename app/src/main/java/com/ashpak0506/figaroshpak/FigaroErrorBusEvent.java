package com.ashpak0506.figaroshpak;

/**
 * @author Andriy
 */
public class FigaroErrorBusEvent {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static class ErrorKey{
        public static String ARTICLE_HEADER_INTERACTION = "ARTICLE_HEADER_INTERACTION";
    }

}
