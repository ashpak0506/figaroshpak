package com.ashpak0506.figaroshpak.util;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.ashpak0506.figaroshpak.R;

/**
 * static method for building Snackbar
 * @author Andriy.
 */
public class MsgUtil {


    public static Snackbar buildErrorSnack(Context context, View view,int errId){
        Snackbar snackbar = Snackbar.make(view,context.getString(errId),Snackbar.LENGTH_LONG);

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            sbView.setBackgroundColor(
                    context.getResources().getColor(R.color.toast_background_color,context.getTheme()));
            textView.setBackgroundColor(
                    context.getResources().getColor(R.color.toast_background_color,context.getTheme()));
        }else {
            //noinspection deprecation
            sbView.setBackgroundColor(
                    context.getResources().getColor(R.color.toast_background_color));
            //noinspection deprecation
            textView.setBackgroundColor(
                    context.getResources().getColor(R.color.toast_background_color));

        }

        return snackbar;
    }

    public static Snackbar buildSnack(Context context, View view,int errId){
        return Snackbar.make(view,context.getString(errId),Snackbar.LENGTH_LONG);
    }

}
